import React, { Component } from "react";
import $ from "jquery";
import InputCustomizado from "./componentes/InputCustomizado";
import PubSub from "pubsub-js";
import TratadorErros from "./TratadorErros";

export default class LivroBox extends Component {
    constructor() {
        super();
        this.state = { lista: [], autores: [] };
    }

    componentDidMount() {
        $.ajax({
            url: "http://localhost:8080/api/livros",
            dataType: "json",
            success: function(resposta) {
                this.setState({ lista: resposta });
            }.bind(this)
        });

        $.ajax({
            url: "http://localhost:8080/api/autores",
            dataType: "json",
            success: function(data) {
                this.setState({ autores: data });
            }.bind(this)
        });

        PubSub.subscribe(
            "atualiza-lista-livros",
            function(topico, novaLista) {
                this.setState({ lista: novaLista });
            }.bind(this)
        );
    }

    render() {
        return (
            <div>
                <div className="header">
                    <h1>Cadastro de livros</h1>
                </div>
                <div className="content" id="content">
                    <FormularioLivro autores={this.state.autores} />
                    <TabelaLivros lista={this.state.lista} />
                </div>
            </div>
        );
    }
}

class FormularioLivro extends Component {
    constructor() {
        super();
        this.state = { titulo: "", autorId: "", preco: "" };
        this.enviaForm = this.enviaForm.bind(this);
    }

    salvaAlteracao(nomeInput,evento){
        var campo = {};
        campo[nomeInput] = evento.target.value;    
        this.setState(campo);   
      }

    enviaForm(evento) {
        evento.preventDefault();

        var titulo = this.state.titulo.trim();
        var preco = this.state.preco.trim();
        var autorId = this.state.autorId;

        $.ajax({
            url: "http://localhost:8080/api/livros",
            contentType: "application/json",
            dataType: "json",
            type: "POST",
            data: JSON.stringify({
                titulo: titulo,
                autorId: autorId,
                preco: preco
            }),
            success: function(novaListagem) {
                PubSub.publish("atualiza-lista-livros", novaListagem);
                this.setState({ titulo: "", autorId: "", preco: "" });
            }.bind(this),
            error: function(resposta) {
                if (resposta.status === 400) {
                    new TratadorErros().publicaErros(resposta.responseJSON);
                }
            },
            beforeSend: function() {
                PubSub.publish("limpa-erros", {});
            }
        });
    }

    render() {
        var autores = this.props.autores.map(function(autor) {
            return (
                <option key={autor.id} value={autor.id}>
                    {autor.nome}
                </option>
            );
        });

        return (
            <div className="pure-form pure-form-aligned">
                <form
                    className="pure-form pure-form-aligned"
                    onSubmit={this.enviaForm}
                    method="post"
                >
                    <InputCustomizado
                        id="titulo"
                        type="text"
                        name="titulo"
                        value={this.state.titulo}
                        onChange={this.salvaAlteracao.bind(this,'titulo')}
                        label="Título:"
                    />
                    <InputCustomizado
                        id="preco"
                        type="text"
                        name="preco"
                        value={this.state.preco}
                        onChange={this.salvaAlteracao.bind(this,'preco')}
                        label="Preço:"
                    />
                    <div className="pure-controls">
                        <label htmlFor="autores">Autor:</label>
                        <select
                            id="autores"
                            value={this.state.autorId}
                            name="autorId"
                            onChange={this.salvaAlteracao.bind(this,'autorId')}
                        >
                            <option value="">Selecione</option>
                            {autores}
                        </select>
                    </div>
                    <div className="pure-control-group">
                        <label></label>
                        <button
                            type="submit"
                            className="pure-button pure-button-primary"
                        >
                            Gravar
                        </button>
                    </div>
                </form>
            </div>
        );
    }
}

class TabelaLivros extends Component {
    render() {
        return (
            <div>
                <table className="pure-table">
                    <thead>
                        <tr>
                            <th>Título</th>
                            <th>Autor</th>
                            <th>Preço</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.props.lista.map(function(livro) {
                            return (
                                <tr key={livro.id}>
                                    <td>{livro.titulo}</td>
                                    <td>{livro.autor.nome}</td>
                                    <td>{livro.preco}</td>
                                </tr>
                            );
                        })}
                    </tbody>
                </table>
            </div>
        );
    }
}
